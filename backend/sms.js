// Download the helper library from https://www.twilio.com/docs/node/install
// Your Account Sid and Auth Token from twilio.com/console
// DANGER! This is insecure. See http://twil.io/secure
const accountSid = 'AC384d4c585d849de87f0815adf24ae098';
const authToken = 'b6016616c4582daeaf84e934de45d5bc';
const client = require('twilio')(accountSid, authToken);

client.messages
  .create({
     body: 'This is the ship that made the Kessel Run in fourteen parsecs?',
     from: '+16043304108',
     to: '+16046262751'
   })
  .then(message => console.log(message.sid));