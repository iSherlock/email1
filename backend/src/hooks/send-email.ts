// Use this hook to manipulate incoming or outgoing data.
// For more information on hooks see: http://docs.feathersjs.com/api/hooks.html
import { Hook, HookContext } from '@feathersjs/feathers';
import nodemailer from 'nodemailer';

export default (options = {}): Hook => {
  return async (context: HookContext) => {
    const { result } = context;
    
    // PUT YOUR SAFE EMAIL RECIPIENTS HERE
    const safeRecipients = ["ian_sherlock@hotmail.ca", "isherlock@langara.ca", "aperrino@langara.ca"];
    
    if ( !safeRecipients.includes(result['to'])) {
      console.log( "BAD: " + result['to']);
      return context;
    }

    console.log( "GOOD: " + result['to']);
    
    // create reusable transporter object using the default SMTP transport
    let transporter = nodemailer.createTransport({
      host: "smtp.sendgrid.net",
      port: 587,
      secure: false,
      requireTLS: true,
      auth: {
        user: "apikey", // SendGrid user
        pass: "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" // SendGrid password
      }
    });
    
        // send mail with defined transport object
    let info = await transporter.sendMail({
      from: '"No Reply" <noreply@494904.xyz>', // PUT YOUR DOMAIN HERE
      to: result.to, // list of receivers
      subject: "Hello " + result.id, // Subject line
      text: "Your id is: " + result.id // plain text body
    });
    
    console.log("Message sent: %s", info.messageId);
    
    return context;
  };
}
