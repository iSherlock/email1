// Use this hook to manipulate incoming or outgoing data.
// For more information on hooks see: http://docs.feathersjs.com/api/hooks.html
import { Hook, HookContext } from '@feathersjs/feathers';

// Download the helper library from https://www.twilio.com/docs/node/install
// Your Account Sid and Auth Token from twilio.com/console
// DANGER! This is insecure. See http://twil.io/secure
const accountSid = 'AC384d4c585d849de87f0815adf24ae098';
const authToken = 'b6016616c4582daeaf84e934de45d5bc';
const client = require('twilio')(accountSid, authToken);
//Will only send to phone numbers taht are verified through my twilio account
export default (options = {}): Hook => {
  return async (context: HookContext) => {
    const { result } = context;
    
    console.log(result);
    console.log(result.to);
    client.messages
      .create({
       body: 'This is the ship that made the Kessel Run in fourteen parsecs? test',
       from: '+16043304108',
       to: result.to
     })
   .then(message => console.log(message.sid));
    return context;
  };
}




